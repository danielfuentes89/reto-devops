# Reto-DevOps CLM

### Reto 1. Dockerize la aplicación
hub image - danielfuentes89/reto-devops:latest

### Reto 2. Docker Compose
1. Nginx que funcione como proxy reverso a nuesta app Nodejs 
2. Asegurar el endpoint /private con auth_basic
(incompleto) Habilitar https y redireccionar todo el trafico 80 --> 443

### Reto 3. Probar la aplicación en cualquier sistema CI/CD
**

### Reto 4. Deploy en kubernetes
kubectl apply -f k8s

kubectl get service (verifica creación de servicios)
kubectl get deployments (verifica creacion de contenedores)
kubectl get pods (verifica pods)

minikube dashboard (interfaz minicube)
minikube service nodeserver (levanta sercicio - service.yml)

### Reto 5. Construir Chart en helm y manejar trafico http(s)
sin construir

### Reto 6. Terraform
sin probar

### Reto 7. Automatiza el despliegue de los retos realizados

