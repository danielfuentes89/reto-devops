resource "kubernetes_role" "read-pods" {
  metadata {
    name = "terraform-k8s-read-pod"
    labels = {
      app = "nodeserver"
    }
  }

  rule {
    api_groups     = [""]
    resources      = ["pods"]
    verbs          = ["get", "list", "watch"]
  }
}