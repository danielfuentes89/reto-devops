# Imagen base
FROM node:alpine

# Crea el directorio de trabajo
WORKDIR /app

# Copia tanto archivo package.json como package-lock.json
COPY package*.json ./

# Instala las dependencias
RUN npm install

# Copia fuente de aplicacion al directorio de trabajo (linea 5)
# Los archivos y directorios mencionados en .dockerignore no se traspasarán a la imagen
COPY . .

# Expone el puerto de APP (puerto interno del contenedor y especificado en app.listen en index.js)
EXPOSE 3000

# selecciona usuario node
USER node

# Comandos para levantar aplicación web express en index
CMD [ "node", "index.js" ]